
# La ville basse de Boğazköy au II^e^ millénaire av. J.-C. Une étude de l’organisation urbaine de la cité-État et de sa restructuration en capitale du royaume hittite

Die Urbanisierung ist ein mehr als sechstausend Jahre altes Phänomen.
Die Vielfalt der Siedlungen, die Stadt genannt werden, macht eine
allgemeingültige Definition ›der‹ Stadt unmöglich. Zu den am
häufigsten genannten Grundzügen einer Stadt zählen die soziale und
ökonomische Vielfalt sowie die Unterscheidung zahlreicher
Aktivitätsräume. Darüber hinaus ist eine Stadt immer als ein Teil eines
Netzwerkes zu verstehen. Städte sind für die Anschaffung und den
Vertrieb von Produkten voneinander abhängig und jede Stadt ist mit den
umliegenden Dörfern eng verbunden. Die Diversität einer Stadt spiegelt
sich auch in ihrer differenzierten, räumlichen Organisation. Sie ist
in verschiedene funktionale Zonen (z.&nbsp;B. Wohn-, Administrativ-,
Religion-, Werkviertel, usw.) unterteilt, die durch ein Straßensystem
verbunden sind.

Diese Arbeit widmet sich der Erforschung des Wohnviertels von Boğazköy
während des 2.&nbsp;Jt. Diese Stadt ist vor allem unter
dem Namen Ḫattuša als Hauptstadt des hethitischen Großreiches bekannt. Die
Erforschung der Stadt konzentrierte sich hauptsächlich auf die
Untersuchung der monumentalen Gebäude, die bisher die wichtigste Rolle
in der Interpretation der Stadt spielten. Trotz dieser generellen
Ausrichtung wurde ein wesentlicher Teil eines Wohnviertels in der
Unterstadt zwischen 1970 und 1977 ausgegraben, ohne dass darüber eine
detaillierte Publikation folgte. Jedoch verändern sich Wohnviertel
schneller als religiöse oder politische Gebäude, so dass sie wichtige,
eigene Aussagemöglichkeiten besitzen, die unersetzbar sind, um eine
Gesellschaft zu rekonstruieren. Aus diesem Grund strebt diese Arbeit
an, durch die Untersuchung eines Wohnviertels, eine bisher weitgehend
neue Facette des urbanen Lebens in Boğazköy darzustellen.

Die zwei letzten Dekaden haben, vor allem am Beispiel der Oberstadt
von Boğazköy, gezeigt, wie groß das Problem der Chronologie in
Boğazköy ist.  Um eine Chronologie für die Unterstadt zu erstellen,
wurde in einem ersten Schritt eine Auswertung von neu aufgenommenen
Keramik-Ensembles der Grabung vorgenommen; in einem zweiten Schritt
wurden diese Ergebnisse mit den Ergebnissen verschiedener
Radiokarbon-Datierungen verknüpft, um eine mögliche Zeitspanne für die
vier Bauschichten zu ermitteln. Die Ergebnisse weisen darauf hin, dass die Siedlung
während des 16. und des 15.&nbsp;Jh. am dichtesten besiedelt
war. Dahingegen deutet die fast komplette Abwesenheit von Datierungen
aus dem 14.&nbsp; und 13.&nbsp;Jh. darauf hin, dass die Siedlung
kleiner war und schlechter erhalten ist.  Dieses Ergebnis steht
in einem deutlichen Kontrast zu den Paradigmen von Bittel
und Neve, aber deckt sich besser mit den Ergebnissen, die in den
letzten zwanzig Jahren gewonnen wurden.

Ein wesentlicher Teil dieser Arbeit widmet sich der Darstellung der
Besiedlung. Jedes Gebäude ist mit einem Plan, einer
Beschreibung und eine Auflistung der Flächen jedes Raumes sowie der
Objekte, die auf Fußboden gefunden wurden, gelistet. Die neu
erstellte Chronologie zeigt, dass die verschiedenen Haustypen nicht
chronologisch unterschieden werden können, und dass es keinen lineraen
Übergang von ›Ackerbürger‹ zu ›Städter‹ gab, wie Neve vermutet
hatte. Durch die Analyse der Straßenverläufe und der Wasserentsorgung
ist deutlich geworden, wie geplant das Stadtviertel angelegt wurde,
und dass die kollektiven Elemente (Straße, Kanalisation) im Laufe der
Zeit nur wenig verändert wurden. Die übergreifende Struktur des
Stadtviertels war offenbar geplant, ohne dass die innere Organisation
der Häuser durch die politische Macht geprägt wurde.

Ein anderer Schwerpunkt der Arbeit liegt auf der Verteilung der
Kleinfunden von der hethitischen Periode, die als Indikator von
Aktivitäten interpretiert und durch räumliche Analyse kontextualisiert
wurden. Die Verteilungskarten der Artefakte belegen vor allem
kleinräumige Aktivitäten in den Häusern. Textilarbeit ist die
sichtbarste Aktivität und die Handwerklichen Aktivitäten  scheinen
sich auf die Ausbesserung und kleinere Arbeiten beschränkt zu haben.
Dies weist hin, dass es sich eher um ein Wohnviertel für Bedienstete
handelte und nicht primär für handwerkliche Produktion oder 
Handel diente. 

Der letzte Teil der Arbeit fasst die Ergebnisse zusammen
und untersucht die Stellung der Unterstadt von Boğazköy im Hinblick
auf die gesamte Region. Im Verlauf des 2. Jt. wurde die Siedlung mit
besseren Straßen und einer aufwendigen Wasserversorgung und
-entsorgung ausgestattet. Die bauliche Monumentalisierung ist in
der hethitischen Periode einzigartig und zeigt den Willen zur
Materialisierung der königlichen Macht. Dieser Schritt ist nur dadurch zu
erklären, dass sich das Netzwerk von Städten von der kārum-Zeit bis zu
der hethitischen Periode wesentlich geändert hat, um die Ressourcen
für diese Bauprojekte bereitzustellen und diese durchzuführen. 

Der Begriff Hauptstadt deutet darauf hin, dass die Stadt innerhalb
eines Netzwerkes untersucht werden muss. Wie dieses Netzwerk gestaltet
war und wie die Städte kommunizierten, ist bisher jedoch für die
hethitischen Epoche kaum erforscht worden. Diese Arbeit strebt an, es
zu ermöglichen.

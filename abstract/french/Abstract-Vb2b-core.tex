\noindent\textbf{Résumé} : L'objectif principal de ce travail est d'exposer les
changements dans l'organisation de la ville de l'âge du bronze, Hattuša, au nord
de l'Anatolie centrale, au moment où elle devient la capitale des Hittites. À
travers une analyse diachronique et multiscalaire du principal quartier
d'habitation de la ville de Hattuša, cette étude pose de façon innovatrice le
problème de l'organisation de la cité-état (1950--1750 av.  J.-C.) et celle de la
capitale de royaume (1700--1200 av. J.-C.), en regardant les liens entre
organisation politique et réalité urbaine.


L'étude débute par une analyse du phénomène urbain et démontre comment la
description et l'analyse d'une ville, en particulier à travers l'optique d'un
quartier d'habitations, permettent de tirer des conclusions sur l'organisation
sociale de celle-ci. Pour replacer la ville de Hattuša dans son contexte, un résumé
critique dresse un portrait précis du climat, de la géographie et de l'évolution
générale de la région méridionale d'Anatolie centrale de la période du
Chalcolithique ancien (6000 av. J.-C.) à la fin du Bronze récent (1200 av. J.-C.).
L'attention est portée ensuite spécifiquement sur le site de Boğazköy, le nom
moderne de la ville antique de Hattuša. Un compendium synthétise le siècle de
recherches archéologiques allemandes de la ville basse et pose les repères
géographiques principaux. La différenciation des zones de la ville
basse insiste sur les lacunes des publications et place la zone du quartier
d'habitation, appelée \textsc{Westterrasse}, au centre des recherches. Enfin la
dernière partie présente quatre problématiques pour étudier la réalité urbaine
grâce à une approche multiscalaire, selon l'échelle de l'archive de la fouille
archéologie, l'échelle de l'unité domestique, l'échelle du quartier et l'échelle
du paysage urbain.

La réflexion du travail porte ensuite sur la méthodologie et propose un examen
détaillé des sources du travail. Une critique du leitmotiv « une fouille
archéologique détruit l'objet de sa recherche » démontre comment une étude
inédite de matériel, doit se plier au principe de scientificité et, entre
autres, au principe de reproductibilité. Le dégagement de faiblesses dans les
méthodes actuelles annonce une méthode innovatrice, appelée « archéologie
numérique littéraire » (en référence au paradigme de la programmation
littéraire), pour combler ces lacunes et dont la mise en œuvre avec le logiciel
R est synthétiquement explicitée. Après ce point méthodologique, le
développement expose les sources à disposition. Une présentation des méthodes de
travail des chercheurs de la ville basse, tout d'abord de Kurt Bittel
(responsable des fouilles 1936--1966) et de Peter Neve (responsable des fouilles
1967--1978) indique la conception des sources (carnets de fouilles, registre des
objets inventoriés, photographies, relevés, coupes stratigraphiques, rapports de
fouilles) et les biais à prendre en considération lors de leur étude. Une note
sur l'accès au matériel, notamment la « crise » de 2012 conclut ce chapitre. 


Une nouvelle chronologie qui remet en question les paradigmes du siècle
précédent pose les bases pour l'étude diachronique.  Jusqu'à présent, la
chronologie de la ville basse se fondait sur différentes fouilles
stratigraphiques dont la synchronisation n'avait été que superficiellement
démontrée par les recherches antérieures. En reprenant tour à tour les chantiers
et leurs stratigraphies respectives (\textsc{Büyükkale, Nordwesthang,
Nordviertel} et \textsc{Unterstadt}), le travail souligne les incohérences du
cadre actuel à la lumière des découvertes récentes. Des travaux inédits sur la
céramique de la la 	\textsc{Westterrasse} (présentés en détail en annexe),
couplés à l'analyse de dates radiocarbones et aux résultats des recherches
récentes, établissent un nouveau  cadre chronologique pour l'évolution de la
ville basse. L'analyse démontre que la phase la mieux attestée de la
\textsc{Westterrasse} ne date pas du 14e--13e siècle, comme c'était admis
jusqu'à présent, mais du 16--15e siècle et que seuls quelques vestiges
sporadiques sont attestés pour la \textsc{Westterrasse} au 14e et 13e siècle.
L'analyse par inférence bayésienne date l'occupation de la \textsc{Westterrasse}
du Bronze ancien entre 2178 et 1967 \emph{cal. bc} (intervalle de confiance à
94,3 \%), la transition Bronze ancien --  Bronze moyen entre 2062 et 1944
\emph{cal. bc} (intervalle de confiance à 91,3 \%), la transition Bronze moyen
Bronze récent entre 1813 et 1617 \emph{cal. bc} (intervalle de confiance à 94,6
\%) et la fin de l’occupation de la 	\textsc{Westterrasse} entre 1607 et 1438
\emph{cal. bc} (intervalle de confiance à 95,4 \%)

Le travail continue ensuite par une description détaillée de l'occupation de la
\textsc{Westterrasse}. Une présentation de chaque bâtiment résume les
informations principales et le matériel qui peut y être associé. Il n'y a pas de
de redondance évidente entre les plans des bâtiments. Les trois quarts des
bâtiments ont une emprise au sol comprise entre 20 et 200 m$^2$ et un quart des
bâtiments ont une dimension supérieure. Alors que la plupart des bâtiments sont
considérés comme des maisons, certains bâtiments étaient visiblement dédiés au
stockage (\textsc{Gebäude 64}, \textsc{Gebäude 84}) et des bâtiments soignés et
spacieux devaient avoir une fonction résidentielle mais aussi administrative
(\textsc{Gebäude 1}, \textsc{Gebäude 28}). L'analyse des fortifications et des
portes d'accès ouvrent l'étude sur les éléments architecturaux non privés.
L'étude des voies de circulation et du système d'évacuation qui se trouve sous
les rues illustre la planification de la \textsc{Westterrasse} entre, la gestion
des aménagements collectif et de l'habitat privé.  Le concept de quartier
(conception privée de l'espace) et celle du district (conception publique de
l'espace), a mis en valeur la gestion des espaces privés et communs et la
médiation nécessaire pour leur cohabitation.

L'enquête présente ensuite une analyse de la totalité des objets de la 	\textsc{Westterrasse}
pour identifier les activités principales. Une étude des contextes considère le
classement taphonomique entre contextes \emph{in situ}, fermés et ouverts comme valeurs
heuristiques pour établir une relation entre objets et activités. Une
classification pragmatique  détaille la catégorisation des objets, visualisée
par des diagrammes de Sankey. Une série de cartes met en relation l'architecture
et la répartition des objets et leur catégorisation pour inférer sur les
activités. Alors que les outils d'administration sont explicitement accaparés
par le pouvoir central au Bronze récent, les petits objets dédiés aux 
travaux d'artisanat sont largement répartis sur tout le site. En revanche,
les objets liés aux activités de subsistance sont étrangement absents du
registre des petits objets et de la répartition.

La dernière partie de la thèse examine la ville basse grâce au
concept de paysage urbain. L'analyse du Bronze moyen délimite pour la première
fois soigneusement l'espace total occupé (25 ha) et propose quelques modèles
pour donner une idée de la population maximale théorique, estimée à 5000 personnes. La
transition du Bronze moyen au Bronze récent est remise dans son contexte
historique et l'auteur revient plus longuement sur les effets du «~saccage~» de la
ville à la fin du Bronze moyen et sur la réoccupation au Bronze récent. Par la suite, l'évolution de l'organisation de la
capitale hittite est reprise, divisé en quatre phases principales et les
changements de la ville basse sont étudiés dans leur globalité. L'auteur
propose une estimation théorique de la population maximale. Enfin, une synthèse
reprend les différences visibles entre l'organisation spatiale des différentes
période  ouvre sur les perspectives pour la reconstruction et la compréhension de
l'organisation économique et politique du réseau de cités-états du Bronze moyen
et du royaume hittite.


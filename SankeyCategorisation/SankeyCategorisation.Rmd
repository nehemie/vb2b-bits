---
title: "« Sankey Categorisation »"
author: "Nehemie Strupler"
date: "Thursday, November 20, 2014"
output: 
    html_document
bibliography: biblio.bib
---

## Introduction

This is a small script about how I made two [Sankey diagrams](http://en.wikipedia.org/wiki/Sankey_diagram) with the libraries `d3Network` and `Riverplot`. My focus was on the result, as always when I try to explore new packages with R and my code should certainly be improved (no warranty). I tried to use Sankey to visualise a typology.

The data are a count of occurrences from 3952 small finds (see [The Data][]) recovered during the 1970-1978 excavations in the Lower City of [Boğazköy](http://www.hattuscha.de). Each type of small finds (like *Spindle whorl*) is assigned to an activity (like *Textile*), itself associated with a category (like *Production of object*). Sankey diagram provides visual representations of the attribution of objects to activities and then to categories. 


### Background 

This idea came out by reading the chapter 4 of [@Verhoeven1999], which I found hard to check. He explains his categorisation of objects (typologie) prior to look at their spatial repartition and find cluster. During the reading, I was wondering how the categorisation influences the spatial repartition. If a group is too small, then you can't found any cluster and the risk is big to find cluster if you lump a lot of things together.

I am following a similar line of thought in my PhD and I came to face similar problems. Verhoeven and I attribute functions to objects in order to explore which activities have been carried? where? with which intensity? before moving forward to a spatial analysis of the activities. With the Sankey diagram you can check how objects are categorised and how much sizes of categories influence on results of the spatial analysis.


### The Data

Data are from my PhD and are **preliminary** listes of small finds. For this reason I decided to provide only summarised data and not the whole data. And yes, this is French without accents (cf `chartr` in [Links][]). The 3 variables of the data are:

  - *Objet*: Objects
  - *ObjetFun*: Functions
  - *ObjetCat*: Categories of activities


I provide the data in three data.frame for an easy shaping. Both packages require a list of the nodes (nodelist) and a data frame with the relations between nodes (linklist).  `KfL.sank.1` is the basis to make links between  *categories* and *functions*. `KfL.sank.2` serve as a basis for the link between *functions* and  *objects*.

  - `KfL.sank.1` is a count of occurrences of *ObjetFun* and *ObjetCat*
  - `KfL.sank.2` is a count of occurrences of *Objet* and *ObjetFun*
  - `KfL.sank.3` is a count of occurrences of *Objet*, *ObjetFun*  and *ObjetCat*
  

### Warranty

  > **NB**
  >
  > This is an ongoing work and all files are posted here for the sake of exposing my methodology and making the post reproducible. DO NOT use the data for anything else, there is NO WARRANTY. It's one of the reasons why only the sums are available and not the whole data.





## D3Sankey


### Read the data
```{r, echo=FALSE, eval=FALSE}
###########################################################
## This R-chunk is for the Author only           
## to prepare the 3 provided table in the post
###########################################################

## From the Original Data: 
## Selection of the two variables and count of the number of occurences for "Objet" and "ObjetFun"
KfL.sank = KfL[1:3952,]
## Run next line to select just one Catgeorie
##KfL.sank =  KfL.sank[KfL.sank$ObjetCat=="PrObjet",] ; KfL.sank=droplevels(KfL.sank)  ## Subsi = 497 | Social 2137 | PrObjet 1318
## Make KfL.sank1
KfL.sank1 =  plyr::count(KfL.sank[,c("ObjetFun","ObjetCat")])
KfL.sank1 = KfL.sank1[order(KfL.sank1$ObjetCat),]

## Make KfL.sank2
KfL.sank2 = plyr::count(KfL.sank[,c("Objet","ObjetFun")])
KfL.sank2 <- dplyr::semi_join(KfL.sank2, KfL.sank1, by="ObjetFun") ## reorder Sank2 according to Sank1

## Make KfL.sank3
KfL.sank3 = plyr::count(KfL.sank[,c("Objet","ObjetFun", "ObjetCat")]) ## For matching with nodelist (see Riverlist)

write.table(KfL.sank1, file="KfL.sank1.csv", row.names=FALSE, sep=",", fileEncoding = "UTF-8")
write.table(KfL.sank2, file="KfL.sank2.csv", row.names=FALSE, sep=",", fileEncoding = "UTF-8")
write.table(KfL.sank3, file="KfL.sank3.csv", row.names=FALSE, sep=",", fileEncoding = "UTF-8")
```


```{r}
#Read the data KfL.sank1 - KfL.sank3
KfL.sank1 = read.csv(file="KfL.sank1.csv", sep=",", fileEncoding = "UTF-8")
KfL.sank2 = read.csv(file="KfL.sank2.csv", sep=",", fileEncoding = "UTF-8")

# Create KfL.sank
KfL.sank = rbind( KfL.sank2 , setNames(KfL.sank1 , names(KfL.sank2)) ) 
# setNames serves to resolve pb of differences in col.names for rbind
```


### libraries
```{r}
# load the libraries
library(d3Network) ; library("rCharts")
```


### Shape the data

A D3 JavaScript Sankey diagram needs:

   1. Nodes (nodelist): a data frame of the unique name of the nodes. 
   2. Links (linklist): a data frame with the links between the nodes. It should include the **Source** and **Target** for each link. **Value**  can be included to specify how close the nodes are to one another.




#### Nodes 

Extract all unique values (unique.level) and attribute to group "1" if value is an Object (x1), "2" for function (x2) and "3" for category (x3)
```{r}
unique.level = unique(c(levels(KfL.sank$Objet),levels(KfL.sank$ObjetFun)))
x1= nlevels(KfL.sank2$Objet) ; x2=nlevels(KfL.sank2$ObjetFun) ; x3=nlevels(KfL.sank1$ObjetCat)
nodelist <- data.frame(name=unique.level, group=c(rep(1,x1),rep(2,x2),rep(3,x3)))
nodelist <- nodelist[order(nodelist$name),] # for the same order as the Source
rownames(nodelist) <- 1:nrow(nodelist)
```


#### Links

KfL.sank is a supperposition of KfL.sank2 and KfL.sank1, i.e of two counts of occurences
`KfL.sank = rbind( KfL.sank2 , setNames(KfL.sank1 , names( KfL.sank2 ) ) )`


```{r,warning=FALSE, message=FALSE}
linklist = KfL.sank
colnames(linklist) <- c("source","target","value") # Rename variable for mapping
linklist = linklist[order(linklist$target),] # order by source
linklist$source = plyr::mapvalues(linklist$source, from =sort(unique.level), to = 0:(length(unique.level)-1)) # rename value according to nodelist
linklist$target = plyr::mapvalues(linklist$target, from =sort(unique.level), to = 0:(length(unique.level)-1)) # rename value according to nodelist

# Remove french accents because they are not displayed
nodelist$name <- chartr("ïêÉàáéèóôç", "ieEaaeeooc", nodelist$name)
# Convert variable from Linklist into numeric for proper functioning of D3Sankey
linklist$source <- as.numeric(as.character(linklist$source))
linklist$target <- as.numeric(as.character(linklist$target))
linklist$value <-  as.numeric(as.character(linklist$value))
```

#### Style

This part is missing because I don't know how to do it (help is much welcome)...#
but colouring should be improved

### Results

```{r,results='asis'}
# Plot
d3Sankey(Links = linklist[,], Nodes = nodelist,
         Source = "source", Target = "target",
         Value = "value", NodeID = "name",
        fontsize = 12, nodeWidth = 50, nodePadding=8, height=1200 ,width = 600, file="Sankey.html", iframe = TRUE, d3Script = "http://d3js.org/d3.v3.min.js" )

```

### Things to do:

   - Manage colours for the plot: colours from `KfL.sank$ObjetFun` should be the same right and left.
   - in the plot the alternate text displays "U+2192" and not a rightwards arrow
   
   
#

## Riverplot

Work realised with the help of the example of Andrew Collier (http://www.r-bloggers.com/plotting-flows-with-riverplot/)

### Libraries
```{r}
# load the libraries
library(riverplot); library(RColorBrewer)
```


### Read the data


```{r, message=FALSE, warning=FALSE}
#Read the data KfL.sank1 - KfL.sank3
KfL.sank1 =read.csv(file="KfL.sank1.csv", sep=",", fileEncoding = "UTF-8")
KfL.sank2 =read.csv(file="KfL.sank2.csv", sep=",", fileEncoding = "UTF-8")
KfL.sank3 =read.csv(file="KfL.sank3.csv", sep=",", fileEncoding = "UTF-8")

# Create KfL.sank
KfL.sank = rbind( KfL.sank2 , setNames(KfL.sank1 , names( KfL.sank2 ) ) ) # rbind with different col.names
```


### Shape the data

#### Links
```{r}
# edgelist
linklist = KfL.sank
for(i in 1:2)linklist[,i]<-as.character(KfL.sank[,i]) # transform linklist in as.character
colnames(linklist) <- c("N1","N2","Value") # Rename variable before mapping
```

#### Nodes
Extract all unique values with a different group and attribute horizontal (x) and vertical (y) value
```{r}
# Nodelist
nodelist = data.frame(ID = unique(c(linklist$N1, linklist$N2)), stringsAsFactors = FALSE)
x1= nlevels(KfL.sank2$Objet) ; x2=nlevels(KfL.sank2$ObjetFun) ; x3=nlevels(KfL.sank1$ObjetCat)
nodelist$x = c(rep(1,x1),rep(2,x2),rep(3,x3))
#nodelist$y = c(seq(1,x1,1), seq(c((x1-x2)/2),c((x1-x2)/2+x2),1), seq(c((x1-x3)/2),c((x1-x3)/2+x3),1))
rownames(nodelist) = nodelist$ID
```




### Styles

#### Creat colours
Make a data.frame nodelist.color with the right colour for each node. I want to map the colour of the category (ObjetCat) for each nodes
```{r}
# Function to attribute a colour to each Functions according to the Categori (KfL.sank3)
for(i in 1:nlevels(KfL.sank3$ObjetCat)){ # Select successively levelsnumber ObjetCat
    palette = paste0(RColorBrewer::brewer.pal(4, "Set1"), "60")  # Basis colour
    n = levels(KfL.sank3$ObjetCat)[i] # Select levels one by one thanks to sequence
    KfL.sank3[KfL.sank3$ObjetCat==n,"color"] <- palette[i+1]  } # Allocate colours (incremental +1) to each object

Objet.col = KfL.sank3[,c("Objet","color")] # Select the variable ID and  color to match with nodelist
colnames(Objet.col) = c("ID","color")  # change colnames for rbind

## Same as before but for objects (KfL.sank1)
for(i in 1:nlevels(KfL.sank1$ObjetCat)){
    palette = paste0(RColorBrewer::brewer.pal(4, "Set1"), "60")
    n = levels(KfL.sank1$ObjetCat)[i] 
    KfL.sank1[KfL.sank1$ObjetCat==n,"color"] <- palette[i+1]  } 

Fun.col = KfL.sank1[,c("ObjetFun","color")]
colnames(Fun.col) = c("ID","color") 

## Add 3 levels of ObjetCat ## 
KfL.sank3= data.frame(ID=levels(KfL.sank3$ObjetCat), color=c(palette[2:c(nlevels(KfL.sank3$ObjetCat)+1)]) )
# The color start from two so I add 1 to numbers of levels of categories

## rbind alltogether
KfL.sank3 <- rbind(Objet.col, Fun.col, KfL.sank3)
row.names(KfL.sank3) <- KfL.sank3$ID

nodelist.color <- merge(nodelist, KfL.sank3, sort=F)
```


#### Map colours to a list
```{r}
styles = lapply(1:nrow(nodelist.color), function(n) {list(col = nodelist.color[n,"color"], lty = 0, textcol = "black")})
names(styles) = nodelist.color$ID
```

## Results
```{r, fig.height=60, fig.width=18}
r <- makeRiver(nodes=nodelist, edges=linklist, node_style=styles)
riverplot(r, nodewidth=2, srt="0", plot_area=0.9, node_margin = 0.1, gravity="center")
```

   
   
## References